<?php
namespace MfoRu\Accounting;

use MfoRu\Accounting\Contracts\AccountRepository;
use MfoRu\Accounting\Contracts\Tariff;
use MfoRu\Accounting\Contracts\TariffSelector;
use MfoRu\Contracts\Billing\Service as BillingService;

class Service
{
    protected $billingService;
    protected $accRepository;
    protected $tariffSelector;
    protected $lastAmountPay;

    function __construct(BillingService $billingSevice, AccountRepository $accountRepository, TariffSelector $tariffSelector)
    {
        $this->billingService = $billingSevice;
        $this->accRepository = $accountRepository;
        $this->tariffSelector = $tariffSelector;
    }

    function run(Anket $anket)
    {
        $tariff = $this->getTariff($anket);
        $summ = $this->calculate($anket, $tariff);
        return $this->pay($anket, $summ);
    }

    function pay(Anket $anket, SummResult $summ)
    {
        $transactionToSys = $this->billingService->getEmptyTransaction();
        $transactionToSys->amount = $summ->mfo;
        $transactionToSys->accountReceiver = $this->accRepository->getId(AccountRepository::TYPE_SYSTEM);
        $transactionToSys->accountSender = $this->accRepository->getId(AccountRepository::TYPE_MFO, $anket->mfo_id);
        $transactionToSys->type = 1;

        $transactionToWebmaster = $this->billingService->getEmptyTransaction();
        $transactionToWebmaster->amount = $summ->webmaster;
        $transactionToWebmaster->accountReceiver = $this->accRepository->getId(AccountRepository::TYPE_WEBMASTER, $anket->webmaster_id);
        $transactionToWebmaster->accountSender = $this->accRepository->getId(AccountRepository::TYPE_SYSTEM);
        $transactionToWebmaster->type = 1;

        $this->billingService->addTransaction($transactionToSys);
        $this->billingService->addTransaction($transactionToWebmaster);
        if($result = $this->billingService->commitTransactions())
        {
            $this->lastAmountPay = $summ;
            return true;
        }

        return false;
    }

    function calculate(Anket $anket, Tariff $tariff)
    {
        $summResult = new SummResult(
            $tariff->getWebmasterSumm(),
            $tariff->getMfoSumm(),
            $tariff->getCommissionSumm()
        );

        return $summResult;
    }

    function getLastAmount()
    {
        return $this->lastAmountPay;
    }

    protected function getTariff(Anket $anket):Tariff
    {
        return $this->tariffSelector->getTariff($anket);
    }
}