<?php
namespace MfoRu\Accounting\Tariff;

use MfoRu\Accounting\Contracts\Tariff as TariffInterface;

class PayOnRequest implements TariffInterface
{
    protected $amountWebmaster;
    protected $amountMfo;

    function __construct($amountWebmaster, $amountMfo)
    {
        if($this->amountWebmaster > $this->amountMfo)
        {
            throw new \Exception('Amount mfo must be exceed amount webmaster');
        }

        $this->amountWebmaster = $amountWebmaster;
        $this->amountMfo = $amountMfo;
    }

    function getWebmasterSumm()
    {
        return $this->amountWebmaster;
    }

    function getMfoSumm()
    {
        return $this->amountMfo;
    }

    function getCommissionSumm()
    {
        return $this->amountMfo - $this->amountWebmaster;
    }
}