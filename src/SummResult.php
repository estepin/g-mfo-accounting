<?php
namespace MfoRu\Accounting;


class SummResult
{
    public $webmaster;
    public $mfo;
    public $commission;

    function __construct($webmasterSumm, $mfoSumm, $commissionSumm)
    {
        $this->webmaster = $webmasterSumm;
        $this->mfo = $mfoSumm;
        $this->commission = $commissionSumm;
    }
}