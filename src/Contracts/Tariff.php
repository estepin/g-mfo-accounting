<?php
namespace MfoRu\Accounting\Contracts;

interface Tariff
{
    function getWebmasterSumm();

    function getMfoSumm();

    function getCommissionSumm();
}