<?php
namespace MfoRu\Accounting\Contracts;


use MfoRu\Accounting\Anket;

interface TariffSelector
{
    function getTariff(Anket $anket);
}