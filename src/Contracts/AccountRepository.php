<?php
namespace MfoRu\Accounting\Contracts;

interface AccountRepository
{
    const TYPE_WEBMASTER = 1;
    const TYPE_MFO = 2;
    const TYPE_SYSTEM = 3;

    function getId($type, $id = false):int;
}